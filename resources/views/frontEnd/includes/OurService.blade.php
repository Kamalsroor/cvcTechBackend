
    @if(count($HomeTopics)>0)
        <section class="content-row-bg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="home-row-head">
                            <h2 class="heading">{{ trans('frontLang.homeContents1Title') }}</h2>
                            <small>{{ trans('frontLang.homeContents1desc') }}</small>
                        </div>
                        <div class="row">
                            <?php
                            $title_var = "title_" . trans('backLang.boxCode');
                            $title_var2 = "title_" . trans('backLang.boxCodeOther');
                            $details_var = "details_" . trans('backLang.boxCode');
                            $details_var2 = "details_" . trans('backLang.boxCodeOther');
                            $slug_var = "seo_url_slug_" . trans('backLang.boxCode');
                            $slug_var2 = "seo_url_slug_" . trans('backLang.boxCodeOther');
                            $section_url = "";
                            ?>
                            @foreach($HomeTopics as $HomeTopic)
                                <?php
                                if ($HomeTopic->$title_var != "") {
                                    $title = $HomeTopic->$title_var;
                                } else {
                                    $title = $HomeTopic->$title_var2;
                                }
                                if ($HomeTopic->$details_var != "") {
                                    $details = $details_var;
                                } else {
                                    $details = $details_var2;
                                }
                                if ($HomeTopic->webmasterSection->$slug_var != "" && Helper::GeneralWebmasterSettings("links_status")) {
                                    if (trans('backLang.code') != env('DEFAULT_LANGUAGE')) {
                                        $section_url = url(trans('backLang.code') . "/" . $HomeTopic->webmasterSection->$slug_var);
                                    } else {
                                        $section_url = url($HomeTopic->webmasterSection->$slug_var);
                                    }
                                } else {
                                    if (trans('backLang.code') != env('DEFAULT_LANGUAGE')) {
                                        $section_url = url(trans('backLang.code') . "/" . $HomeTopic->webmasterSection->name);
                                    } else {
                                        $section_url = url($HomeTopic->webmasterSection->name);
                                    }
                                }

                                if ($HomeTopic->$slug_var != "" && Helper::GeneralWebmasterSettings("links_status")) {
                                    if (trans('backLang.code') != env('DEFAULT_LANGUAGE')) {
                                        $topic_link_url = url(trans('backLang.code') . "/" . $HomeTopic->$slug_var);
                                    } else {
                                        $topic_link_url = url($HomeTopic->$slug_var);
                                    }
                                } else {
                                    if (trans('backLang.code') != env('DEFAULT_LANGUAGE')) {
                                        $topic_link_url = route('FrontendTopicByLang', ["lang" => trans('backLang.code'), "section" => $HomeTopic->webmasterSection->name, "id" => $HomeTopic->id]);
                                    } else {
                                        $topic_link_url = route('FrontendTopic', ["section" => $HomeTopic->webmasterSection->name, "id" => $HomeTopic->id]);
                                    }
                                }

                                ?>
                                <div class="col-lg-4">
                                    <div class="vc-services-card">
                                        <div class="content">
                                            <h4>
                                                @if($HomeTopic->icon !="")
                                                    <i class="fa {!! $HomeTopic->icon !!} "></i>&nbsp;
                                                @endif
                                                {{ $title }}
                                            </h4>
                                            @if($HomeTopic->photo_file !="")
                                                <img src="{{ URL::to('uploads/topics/'.$HomeTopic->photo_file) }}"
                                                    alt="{{ $title }}"/>
                                            @endif
                                            {{--Additional Feilds--}}
                                            @if(count($HomeTopic->webmasterSection->customFields) >0)
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div>
                                                            <?php
                                                            $cf_title_var = "title_" . trans('backLang.boxCode');
                                                            $cf_title_var2 = "title_" . trans('backLang.boxCodeOther');
                                                            ?>
                                                            @foreach($HomeTopic->webmasterSection->customFields as $customField)
                                                                <?php
                                                                if ($customField->$cf_title_var != "") {
                                                                    $cf_title = $customField->$cf_title_var;
                                                                } else {
                                                                    $cf_title = $customField->$cf_title_var2;
                                                                }


                                                                $cf_saved_val = "";
                                                                $cf_saved_val_array = array();
                                                                if (count($HomeTopic->fields) > 0) {
                                                                    foreach ($HomeTopic->fields as $t_field) {
                                                                        if ($t_field->field_id == $customField->id) {
                                                                            if ($customField->type == 7) {
                                                                                // if multi check
                                                                                $cf_saved_val_array = explode(", ", $t_field->field_value);
                                                                            } else {
                                                                                $cf_saved_val = $t_field->field_value;
                                                                            }
                                                                        }
                                                                    }
                                                                }

                                                                ?>

                                                                @if(($cf_saved_val!="" || count($cf_saved_val_array) > 0) && ($customField->lang_code == "all" || $customField->lang_code == trans('backLang.boxCode')))
                                                                    @if($customField->type ==12)
                                                                        {{--Vimeo Video Link--}}
                                                                    @elseif($customField->type ==11)
                                                                        {{--Youtube Video Link--}}
                                                                    @elseif($customField->type ==10)
                                                                        {{--Video File--}}
                                                                    @elseif($customField->type ==9)
                                                                        {{--Attach File--}}
                                                                    @elseif($customField->type ==8)
                                                                        {{--Photo File--}}
                                                                    @elseif($customField->type ==7)
                                                                        {{--Multi Check--}}
                                                                        <div class="row field-row">
                                                                            <div class="col-lg-3">
                                                                                {!!  $cf_title !!} :
                                                                            </div>
                                                                            <div class="col-lg-9">
                                                                                <?php
                                                                                $cf_details_var = "details_" . trans('backLang.boxCode');
                                                                                $cf_details_var2 = "details_en" . trans('backLang.boxCodeOther');
                                                                                if ($customField->$cf_details_var != "") {
                                                                                    $cf_details = $customField->$cf_details_var;
                                                                                } else {
                                                                                    $cf_details = $customField->$cf_details_var2;
                                                                                }
                                                                                $cf_details_lines = preg_split('/\r\n|[\r\n]/', $cf_details);
                                                                                $line_num = 1;
                                                                                ?>
                                                                                @foreach ($cf_details_lines as $cf_details_line)
                                                                                    @if (in_array($line_num,$cf_saved_val_array))
                                                                                        <span class="badge">
                                                                    {!! $cf_details_line !!}
                                                                </span>
                                                                                    @endif
                                                                                    <?php
                                                                                    $line_num++;
                                                                                    ?>
                                                                                @endforeach
                                                                            </div>
                                                                        </div>
                                                                    @elseif($customField->type ==6)
                                                                        {{--Select--}}
                                                                        <div class="row field-row">
                                                                            <div class="col-lg-3">
                                                                                {!!  $cf_title !!} :
                                                                            </div>
                                                                            <div class="col-lg-9">
                                                                                <?php
                                                                                $cf_details_var = "details_" . trans('backLang.boxCode');
                                                                                $cf_details_var2 = "details_en" . trans('backLang.boxCodeOther');
                                                                                if ($customField->$cf_details_var != "") {
                                                                                    $cf_details = $customField->$cf_details_var;
                                                                                } else {
                                                                                    $cf_details = $customField->$cf_details_var2;
                                                                                }
                                                                                $cf_details_lines = preg_split('/\r\n|[\r\n]/', $cf_details);
                                                                                $line_num = 1;
                                                                                ?>
                                                                                @foreach ($cf_details_lines as $cf_details_line)
                                                                                    @if ($line_num == $cf_saved_val)
                                                                                        {!! $cf_details_line !!}
                                                                                    @endif
                                                                                    <?php
                                                                                    $line_num++;
                                                                                    ?>
                                                                                @endforeach
                                                                            </div>
                                                                        </div>
                                                                    @elseif($customField->type ==5)
                                                                        {{--Date & Time--}}
                                                                        <div class="row field-row">
                                                                            <div class="col-lg-3">
                                                                                {!!  $cf_title !!} :
                                                                            </div>
                                                                            <div class="col-lg-9">
                                                                                {!! date('Y-m-d H:i:s', strtotime($cf_saved_val)) !!}
                                                                            </div>
                                                                        </div>
                                                                    @elseif($customField->type ==4)
                                                                        {{--Date--}}
                                                                        <div class="row field-row">
                                                                            <div class="col-lg-3">
                                                                                {!!  $cf_title !!} :
                                                                            </div>
                                                                            <div class="col-lg-9">
                                                                                {!! date('Y-m-d', strtotime($cf_saved_val)) !!}
                                                                            </div>
                                                                        </div>
                                                                    @elseif($customField->type ==3)
                                                                        {{--Email Address--}}
                                                                        <div class="row field-row">
                                                                            <div class="col-lg-3">
                                                                                {!!  $cf_title !!} :
                                                                            </div>
                                                                            <div class="col-lg-9">
                                                                                {!! $cf_saved_val !!}
                                                                            </div>
                                                                        </div>
                                                                    @elseif($customField->type ==2)
                                                                        {{--Number--}}
                                                                        <div class="row field-row">
                                                                            <div class="col-lg-3">
                                                                                {!!  $cf_title !!} :
                                                                            </div>
                                                                            <div class="col-lg-9">
                                                                                {!! $cf_saved_val !!}
                                                                            </div>
                                                                        </div>
                                                                    @elseif($customField->type ==1)
                                                                        {{--Text Area--}}
                                                                    @else
                                                                        {{--Text Box--}}
                                                                        <div class="row field-row">
                                                                            <div class="col-lg-3">
                                                                                {!!  $cf_title !!} :
                                                                            </div>
                                                                            <div class="col-lg-9">
                                                                                {!! $cf_saved_val !!}
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                @endif
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                            {{--End of -- Additional Feilds--}}
                                            <p class="text-justify">{{ str_limit(strip_tags($HomeTopic->$details), $limit = 400, $end = '...') }}
                                                &nbsp; <a href="{{ $topic_link_url }}">{{ trans('frontLang.readMore') }}
                                                    <i
                                                            class="fa fa-caret-{{ trans('backLang.right') }}"></i></a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="more-btn">
                            <a href="{{ url($section_url) }}" class="btn btn-theme"><i
                                        class="fa fa-angle-left"></i>&nbsp; {{ trans('frontLang.viewMore') }}
                                &nbsp;<i
                                        class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    @endif
